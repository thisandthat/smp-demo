import Vue from 'vue'
import Router from 'vue-router'
import {globalStore} from '@/globalStore'
import Hello from '@/components/Hello'
import UserCard from '@/components/UserCard'

Vue.use(Router)

const router = new Router({
	routes: [
		{
			path: '/',
			name: 'Hello',
			component: Hello,
		},
		{
			path: '/usercard',
			name: 'usercard',
			component: UserCard,
		},
	]
})

router.beforeEach((to, from, next) => {
	console.log('before each:', to.path)
	let processed
	switch (to.path) {
		case '/':
			processed = 'Home'
			break
		// case '/admins':
		// 	//no links to this route so shouldn't be needed
		// case '/admins/create':
		// case '/admins/edit':
		// 	processed = 'Managing Admins'
		// 	break
		// case '/plans':
		// case '/plans/create':
		// case '/plans/edit':
		// 	processed = 'Managing Plans'
		// 	break
		// case '/discounts':
		// case '/discounts/create':
		// case '/discounts/edit':
		// 	processed = 'Manging Discounts'
		// 	break
		// case '/codes':
		// case '/codes/generate':
		// case '/codes/recall':
		// 	processed = 'Managing Discount Codes'
		// 	break
		// case '/graphs':
		// 	processed = 'Graphs'
		// 	break
		// case '/tags':
		// case '/tags/create':
		// case '/tags/remove':
		// 	processed = 'Tags'
		// 	break
		// case '/settings':
		// 	processed = 'Settings'
		// 	break
	}
	globalStore.title = processed
	next()
})

export default router