import axios from 'axios'
import {globalStore} from '@/globalStore'
import CryptoJS from 'crypto-js'

export const mergePatch = 'application/merge-patch+json; charset=UTF-8'
export const contentType = 'Content-Type'

export const httpConf = {
	baseURL: '',
	headers: {
		'Content-Type': 'application/json; charset=UTF-8',
		// 'Access-Control-Allow-Origin': '*'
		// Authorization: 'abcdefgh1234'
	},
}

export var HTTP = axios.create(httpConf)

const saveConf = function (address, appName) {
	globalStore.serverAddress = address
	globalStore.appName = appName
	httpConf.baseURL = address + 'app/' + appName
	HTTP = axios.create(httpConf)
	httpConf.baseURL = address + 'stats/' + appName
}

export { saveConf }

const calculateHash = function(payload, when) {
	let stringified = ''
	if (payload !== '') {
		stringified = JSON.stringify(payload)
	}
	let hash = CryptoJS.HmacSHA256(stringified + when, globalStore.auth).toString(CryptoJS.enc.Hex)
	console.log(hash)
	return hash
}

export { calculateHash }

const calculateUserHash = function(payload, when, userHash) {
	let stringified = ''
	if (payload !== '') {
		stringified = JSON.stringify(payload)
	}
	let hash = CryptoJS.HmacSHA256(stringified + when, userHash).toString(CryptoJS.enc.Hex)
	console.log(hash)
	return hash
}

export { calculateUserHash }

const calculateLoginHash = function(password, when) {
	let hash = CryptoJS.HmacSHA256(when, password).toString(CryptoJS.enc.Hex)
	console.log(hash)
	return hash
}

export { calculateLoginHash }

const getHTTPtime = function () {
	let now = new Date()
	return now.toUTCString()
}

const setWhen = function (config) {
	let when = getHTTPtime()
	console.log(when)
	config.headers['When'] = when
	return when
}

export { setWhen }